VIN Decode Service
=============

The service uses [DecodeThis](https://www.decodethis.com/) service to decode VIN. You can change api key `DECODE_THIS_API_KEY`  in `settings.py`

### Installing app

#### 1. Requirements
- docker (https://docs.docker.com/install/)
- docker-compose (https://docs.docker.com/compose/install/)
    

#### 2. Build
Run the following command

`docker-compose up -d`

This command builds docker containers and runs docker containers in background and starts dev server at [http://localhost:8000](http://localhost:8000)

#### 3. Create admin user 

Run the following command to create super user who can login to the admin site

`docker-compose run web python  manage.py createsuperuser`

This command will ask admin user details. Enter your desired username/password/email.

#### 4. Create api client and token

To use thi API you need auth token. Create api token via admin panel:

Go to [http://localhost:8000/admin/](http://localhost:8000/admin/) and login with superuser account you created in the previous step.

1. Add new user [http://localhost:8000/admin/auth/user/add/](http://localhost:8000/admin/auth/user/add/)     

2. Create api token for user [http://localhost:8000/admin/authtoken/token/add/](http://localhost:8000/admin/authtoken/token/add/)


### API Documentation

This app uses drf-yasg (https://drf-yasg.readthedocs.io/en/stable/readme.html) to generate the API documentation site.

A JSON view of API specification at [http://localhost:8000/swagger.json](http://localhost:8000/swagger.json)

A YAML view of API specification at [http://localhost:8000/swagger.yaml](http://localhost:8000/swagger.yaml)

A swagger-ui view of API specification at [http://localhost:8000/swagger/](http://localhost:8000/swagger/)

A ReDoc view of API specification at [http://localhost:8000/redoc/](http://localhost:8000/redoc/)


### Running service locally

#### Starting docker containers 

```docker-compose start```

#### Stopping docker containers

```docker-compose stop``` 



### Running tests
To run the unit tests run following command

`docker-compose run  web python manage.py test` 
