from __future__ import print_function

from django.contrib.auth.models import User
from django.db.utils import IntegrityError

username = 'admin'
email = 'admin@admin.admin'
password = 'passwordadmin'

api_client_username = 'testclient'
api_client_email = 'testclient@test.test'
api_client_password = 'passwordtestclient'

try:
    User.objects.create_superuser(username, email, password)
    User.objects.create_user(api_client_username, api_client_email, api_client_password)
except IntegrityError:
    print("User '%s <%s>' already exists" % (username, email))
else:
    print("Created superuser '%s <%s>' with password '%s'" % (username, email, password))
