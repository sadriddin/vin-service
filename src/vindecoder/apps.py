from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class VinDecoderConfig(AppConfig):
    name = 'vindecoder'
    verbose_name = _("VIN Decoder")
