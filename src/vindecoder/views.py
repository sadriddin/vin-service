from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from vindecoder.decoder import DecodeException, InvalidVinNumberError, NotFoundVinNumberException
from vindecoder.models import Vehicle
from vindecoder.serializers import VehicleSerializer

decode_response = openapi.Response('', schema=VehicleSerializer)


@swagger_auto_schema(method='get', responses={
    200: decode_response,
    404: 'VIN Number not found',
    400: 'Invalid VIN number',
    500: 'Internal server error'})
@api_view(['GET'])
def vin_decode(request, vin_number, format=None):
    """
    Returns vehicle details by decoding the provided VIN number
    """
    try:
        model = Vehicle.find_by_vin_number(vin_number)
    except InvalidVinNumberError:
        return Response(data='Invalid VIN number', status=status.HTTP_400_BAD_REQUEST)
    except NotFoundVinNumberException:
        return Response(data='VIN number not found', status=status.HTTP_404_NOT_FOUND)
    except DecodeException:
        return Response(data='Internal server error', status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    serializer = VehicleSerializer(model)
    return Response(serializer.data)
