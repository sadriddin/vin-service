from rest_framework import serializers

from vindecoder.models import Vehicle


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        exclude = ('id', 'vin_number', 'created_at', 'updated_at')
