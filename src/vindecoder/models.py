from django.db import models

# Create your models here.
from vindecoder import decoder


class Vehicle(models.Model):
    vin_number = models.CharField(max_length=17, unique=True)
    year = models.IntegerField(null=True, blank=True)
    make = models.CharField(max_length=128, null=True, blank=True)
    model = models.CharField(max_length=128, null=True, blank=True)
    body_type = models.CharField(max_length=128, null=True, blank=True)
    color = models.CharField(max_length=128, null=True, blank=True)
    length = models.FloatField(null=True, blank=True)
    height = models.FloatField(null=True, blank=True)
    width = models.FloatField(null=True, blank=True)
    distance_unit = models.CharField(max_length=16, null=True, blank=True)
    weight = models.FloatField(null=True, blank=True)
    mass_unit = models.CharField(max_length=16, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.vin_number

    @classmethod
    def find_by_vin_number(cls, vin_number: str):
        try:
            return cls.objects.get(vin_number=vin_number)
        except cls.DoesNotExist:
            decoded_data = decoder.decode(vin_number)
            return cls.save_decoded_data(vin_number, decoded_data)

    @classmethod
    def save_decoded_data(cls, vin_number, data):
        model = cls.objects.create(**data)
        model.vin_number = vin_number
        model.save()
        return model
