import requests
from django.conf import settings
from abc import ABC, abstractmethod


class DecodeException(Exception):
    """ Base decode exception """
    pass


class InvalidVinNumberError(DecodeException):
    """ Use when vin number is invalid """
    pass


class NotFoundVinNumberException(DecodeException):
    """ Use when vin number not found in remote provider """
    pass


class BaseDecoder(ABC):
    @abstractmethod
    def decode(self, vin_number: str) -> dict:
        """Returns decoded data by vin number

        Returns:
            A dict with following keys
              - make: `string`
              - model: `string`
              - year: `int`
              - body_type: `string`
              - color: `string`
              - length: `float`
              - height: `float`
              - width: `float`
              - distance_unit: `string`
              - weight: `float`
              - mass_unit: `string`

            If field value is empty or field does not exist in remote provider data, field value will be `None`

            Example:

            {
               'make': 'BMW',

               'model': 'X6',

               'year': 2008,

               'body_type': 'Mid-size luxury crossover SUV',

               'color': None,

               'length': 192.0,

               'height': 66.8,

               'width': 77.9,

               'distance_unit': 'in',

               'weight': 4629.7,

               'mass_unit': 'lb',
            }

        Raises:
            DecodeException: An error occurred when decoding data
            InvalidVinNumberError: When invalid VIN number provided
            NotFoundVinNumberException: VIN was well-formed but not found in remote provider
        """
        pass

    def make_empty_vehicle_data(self):
        """Returns vehicle data dict with empty values"""
        return {
            'make': None,
            'model': None,
            'year': None,
            'body_type': None,
            'color': None,
            'length': None,
            'height': None,
            'width': None,
            'distance_unit': None,
            'weight': None,
            'mass_unit': None,
        }

    @classmethod
    def cast_fields(cls, result):
        # float fields
        for f in ('length', 'width', 'height', 'weight'):
            result[f] = float(result[f]) if result[f] is not None else result[f]
        # integer fields
        for f in ('year',):
            result[f] = int(result[f]) if result[f] is not None else result[f]
        return result


class DecodeThisDecoder(BaseDecoder):
    decode_base_url = 'https://www.decodethis.com/webservices/decodes'

    DECODE_STATUS_SUCCESS = 'SUCCESS'
    DECODE_STATUS_INVALID_VIN = 'VINERR'
    DECODE_STATUS_CHECK_ERROR = 'CHECKERR'
    DECODE_STATUS_NOT_FOUND_VIN = 'NOTFOUND'
    DECODE_STATUS_INVALID_API_KEY = 'SECERR'
    DECODE_STATUS_INVALID_PARAM_ERROR = 'PARAMERR'

    def __init__(self, api_key):
        self.api_key = api_key

    def build_decode_url(self, vin_number: str) -> str:
        """ Builds decode url """
        return self.decode_base_url + '/' + vin_number + '/' + self.api_key + '/1'

    def decode(self, vin_number: str):
        try:
            response = requests.get(self.build_decode_url(vin_number))
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            raise DecodeException('Cannot connect decode provider')

        #
        # response data
        #
        # {
        #     "decode" : {
        #         "status" : "SUCCESS",
        #         "Valid" : "True",
        #         "vehicle" : [
        #             {
        #                 "body" : "CONVERTIBLE 2-DR",
        #                 "driveline" : "RWD",
        #                 "engine" : "3.5L V6 SOHC 24V",
        #                 "Equip" : [
        #                     {
        #                         "name" : "Engine Type",
        #                         "unit" : "",
        #                         "value" : "3.5L V6 SOHC 24V"
        #                     },
        #                     ...
        #                 ],
        #                 "id": "110153",
        #                 "make": "Plymouth",
        #                 "model": "Prowler",
        #                 "trim": "Base",
        #                 "year": "1997"
        #
        #             }
        #         ],
        #         "version" : "DECODE",
        #         "VIN" : "1P3EW65F4VV300946"
        #     }
        # }
        #
        data = response.json()['decode']
        if data['status'] == self.DECODE_STATUS_SUCCESS:
            result = self.make_empty_vehicle_data()
            # index by name by field
            equip_data = {r['name']: r for r in data['vehicle'][0]['Equip']}
            key_map = {
                'make': 'Make',
                'model': 'Model',
                'year': 'Model Year',
                'body_type': 'Body Style',
                'color': 'Exterior Color',
                'length': 'Overall Length',
                'height': 'Overall Height',
                'width': 'Overall Width'
            }
            for k, equip_key in key_map.items():
                value = equip_data.get(equip_key, None)
                if value is not None:
                    result[k] = value['value']

            # there are 2 possible keys for weight
            weight = equip_data.get('Curb Weight-manual', equip_data.get('Curb Weight-automatic', None))
            if weight is not None:
                result['weight'] = weight['value']
                result['mass_unit'] = weight['unit']

            distance_unit = equip_data.get('Overall Height', equip_data.get('Overall Width', None))
            if distance_unit is not None:
                result['distance_unit'] = distance_unit['unit']

            # cast number fields
            result = self.cast_fields(result)

            return result
        elif data['status'] == self.DECODE_STATUS_INVALID_VIN or data['status'] == self.DECODE_STATUS_CHECK_ERROR:
            raise InvalidVinNumberError
        elif data['status'] == self.DECODE_STATUS_NOT_FOUND_VIN:
            raise NotFoundVinNumberException
        elif data['status'] == self.DECODE_STATUS_INVALID_API_KEY:
            raise DecodeException('Invalid api key')
        elif data['status'] == self.DECODE_STATUS_INVALID_PARAM_ERROR:
            raise DecodeException('Invalid request param')

        raise DecodeException('Unexpected response from decoder service')


decoder = DecodeThisDecoder(settings.DECODE_THIS_API_KEY)


def decode(vin_number: str) -> dict:
    """Returns decoded data by vin number

    Returns:
        A dict with following keys
          - make: `string`
          - model: `string`
          - year: `int`
          - body_type: `string`
          - color: `string`
          - length: `float`
          - height: `float`
          - width: `float`
          - distance_unit: `string`
          - weight: `float`
          - mass_unit: `string`

        If field value is empty or field does not exist, field value will be returned as `None`

        Example:

        {
           'make': 'BMW',

           'model': 'X6',

           'year': 2008,

           'body_type': 'Mid-size luxury crossover SUV',

           'color': None,

           'length': 192.0,

           'height': 66.8,

           'width': 77.9,

           'distance_unit': 'in',

           'weight': 4629.7,

           'mass_unit': 'lb',
        }

    Raises:
        DecodeException: An error occurred when decoding data
        InvalidVinNumberError: When invalid VIN number provided
        NotFoundVinNumberException: VIN was well-formed but not found in remote provider
    """
    return decoder.decode(vin_number)
