from django.contrib.auth.models import User
from django.db import IntegrityError
from django.forms import model_to_dict
from django.test import TestCase

# Create your tests here.
from django.urls import reverse

from vindecoder import decoder
from vindecoder.models import Vehicle
from rest_framework.authtoken.models import Token

valid_vin_number = '1P3EW65F4VV300946'
invalid_vin_number = 'invaliddecodnumber'

valid_decoded_data = {
    'make': 'Plymouth',
    'model': 'Prowler',
    'year': 1997,
    'body_type': 'CONVERTIBLE 2-DR',
    'color': None,
    'length': 165.3,
    'height': 50.9,
    'width': 76.5,
    'distance_unit': 'in.',
    'weight': 2862.,
    'mass_unit': 'lbs',
}


class DecoderTests(TestCase):
    def test_raise_on_invalid_vin_number(self):
        """
        If vin number is invalid, an appropriate error is raised
        """
        with self.assertRaises(decoder.InvalidVinNumberError):
            decoder.decode(invalid_vin_number)

    def test_raise_on_not_found_vin_number(self):
        """
        If vin number is invalid, an appropriate error is raised
        """
        with self.assertRaises(decoder.NotFoundVinNumberException):
            decoder.decode('11111')

    def test_decoding_vin_number(self):
        """
        Check decoded data
        """
        self.assertEqual(decoder.decode(valid_vin_number), valid_decoded_data)


class VinDataModelTests(TestCase):
    def test_saving_decoded_data(self):
        """
        All decoded data must be saved in db
        """
        Vehicle.find_by_vin_number(valid_vin_number)
        model = Vehicle.objects.get(vin_number=valid_vin_number)
        model_dict = model_to_dict(model)
        del model_dict['id']
        del model_dict['vin_number']
        self.assertEquals(model_dict, valid_decoded_data)


api_client_username = 'testclient'
api_client_email = 'testclient@test.test'
api_client_password = 'passwordtestclient'


def create_test_client():
    try:
        return User.objects.create_user(api_client_username, api_client_email, api_client_password)
    except IntegrityError:
        return User.objects.get(username=api_client_username)
        pass


class VinApiTests(TestCase):
    def test_authorized_user(self):
        user = create_test_client()
        token = Token.objects.create(user=user)
        response = self.client.get(reverse('vindecoder:decode', args=(valid_vin_number,)),
                                   HTTP_AUTHORIZATION='Token ' + token.key)
        self.assertEqual(response.status_code, 200)

    def test_unauthorized_user(self):
        response = self.client.get(reverse('vindecoder:decode', args=(valid_vin_number,)))
        self.assertEqual(response.status_code, 401)

    def test_invalid_vin_number(self):
        user = create_test_client()
        token = Token.objects.create(user=user)
        response = self.client.get(reverse('vindecoder:decode', args=(invalid_vin_number,)),
                                   HTTP_AUTHORIZATION='Token ' + token.key)
        self.assertEqual(response.status_code, 400)
