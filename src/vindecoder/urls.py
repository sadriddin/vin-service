from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from vindecoder import views

app_name = 'vindecoder'

urlpatterns = [
    url(r'^decode/(?P<vin_number>[a-zA-Z0-9]+)$', views.vin_decode, name='decode'),

]

urlpatterns = format_suffix_patterns(urlpatterns)
