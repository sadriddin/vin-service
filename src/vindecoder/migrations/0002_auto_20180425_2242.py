# Generated by Django 2.0.4 on 2018-04-25 17:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vindecoder', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='vindata',
            old_name='dimension_unit',
            new_name='distance_unit',
        ),
        migrations.RenameField(
            model_name='vindata',
            old_name='weight_unit',
            new_name='mass_unit',
        ),
    ]
