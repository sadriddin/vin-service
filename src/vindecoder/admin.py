from django.contrib import admin

# Register your models here.
from vindecoder.models import Vehicle

admin.site.register(Vehicle)
